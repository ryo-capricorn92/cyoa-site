import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';

import { getUser } from '../../reducers/user';

const Container = styled.div.withConfig({
  displayName: 'Container',
})`
  align-items: center;
  display: flex;
  border-left: 1px ridge #444;
  height: 42px;
  margin: 0 10px;
  padding-left: 10px;
`;

const Avatar = styled.img.withConfig({
  displayName: 'Avatar',
})`
  border-radius: 16px;
  margin-right: 10px;
`;

const User = ({ avatarURL, username }) => (
  <Container>
    <Avatar src={avatarURL} alt="avatar" width="32px" height="32px" />
    <span>{username}</span>
  </Container>
);

User.propTypes = {
  avatarURL: PropTypes.string.isRequired,
  username: PropTypes.string.isRequired,
};

const mapStateToProps = state => ({
  avatarURL: getUser(state).avatarURL,
  username: getUser(state).username,
});

export default connect(mapStateToProps)(User);
