import { Chrome, Nav, NavItem, NavItemIcon, NavItemText } from '@zendeskgarden/react-chrome';
import { ThemeProvider } from '@zendeskgarden/react-theming';
import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

import { STEPS, STEP_ICONS } from '../../constants';
import { getLoggedIn } from '../../reducers';
import theme from '../../styles/theme';

const Icon = styled.div.withConfig({
  displayName: 'Icon',
})`
  display: flex;
  justify-content: center;
  align-items: center;
`;

const RespectfulLink = styled(Link).withConfig({
  displayName: 'RespectfulLink',
})`
  display: flex;
  color: inherit;

  button {
    flex: 1 0 auto;
  }
`;

const WrappedNavItem = styled(NavItem).withConfig({
  displayName: 'WrappedNavItem',
})`
  &:disabled,
  &:disabled:hover {
    background: transparent;
    color: #bbbbbb;
    cursor: default;
    opacity: 0.6;
  }
`;

const Item = ({ currentStep, disabled, step }) => {
  if (disabled) {
    return (
      <WrappedNavItem disabled>
        <NavItemIcon>
          <Icon>
            <i className={`fa ${STEP_ICONS[step]} fa-lg`} aria-hidden="true" />
          </Icon>
        </NavItemIcon>
        <NavItemText>{step}</NavItemText>
      </WrappedNavItem>
    );
  }

  return (
    <RespectfulLink to={step.toLowerCase()}>
      <WrappedNavItem current={step === currentStep}>
        <NavItemIcon>
          <Icon>
            <i className={`fa ${STEP_ICONS[step]} fa-lg`} aria-hidden="true" />
          </Icon>
        </NavItemIcon>
        <NavItemText>{step}</NavItemText>
      </WrappedNavItem>
    </RespectfulLink>
  );
};

Item.defaultProps = {
  disabled: false,
};

Item.propTypes = {
  currentStep: PropTypes.string.isRequired,
  disabled: PropTypes.bool,
  step: PropTypes.string.isRequired,
};

const Navigation = ({ loggedIn, step }) => (
  <ThemeProvider {...{ theme }} >
    <Chrome>
      <Nav expanded>
        <Item
          currentStep={step}
          step={STEPS.HOME}
        />
        <Item
          currentStep={step}
          disabled={!loggedIn}
          step={STEPS.STATS}
        />
        <Item
          currentStep={step}
          disabled={!loggedIn}
          step={STEPS.STORY}
        />
        <Item
          currentStep={step}
          disabled={!loggedIn}
          step={STEPS.PUBLISH}
        />
        <Item
          currentStep={step}
          disabled={!loggedIn}
          step={STEPS.SETTINGS}
        />
      </Nav>
    </Chrome>
  </ThemeProvider>
);

Navigation.propTypes = {
  loggedIn: PropTypes.bool.isRequired,
  step: PropTypes.string.isRequired,
};

const mapStateToProps = state => ({
  loggedIn: getLoggedIn(state),
});

export default connect(mapStateToProps)(Navigation);
