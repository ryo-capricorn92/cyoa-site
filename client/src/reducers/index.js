import { combineReducers } from 'redux';

import { STEPS } from '../constants';

import user from './user';

const loggedIn = (state = false, action) => {
  switch (action.type) {
    case 'LOGIN':
      return true;
    case 'LOGOUT':
      return false;
    default:
      return state;
  }
};

const step = (state = STEPS.HOME, action) => {
  switch (action.type) {
    case 'SET_STEP':
      return action.step;
    default:
      return state;
  }
};

export default combineReducers({
  loggedIn,
  step,
  user,
});

export const getLoggedIn = state => state.loggedIn;
export const getStep = state => state.step;
