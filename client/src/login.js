const { LoginWithDiscord } = require('./util/lwd');

const discord = new LoginWithDiscord({
  cache: true,
});

discord.onlogin = async () => discord.fetchUser().then((data) => {
  this.a.setUser(data);
  this.a.setLogin();
});

discord.onlogout = async () => {
  this.a.setLogout();
};

export async function login() {
  let URI = window.location.origin;
  if (process.env.NODE_ENV === 'production') {
    URI = 'https://cyoa-bot.herokuapp.com/';
  }
  await discord.login(process.env.REACT_APP_DISCORD_ID, process.env.REACT_APP_DISCORD_SECRET, URI, ['identify', 'email', 'guilds']);
}

export async function logout() {
  await discord.logout();
}

export default discord;
