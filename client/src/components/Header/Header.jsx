import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';

import { getLoggedIn } from '../../reducers';
import { login, logout } from '../../login';

import User from './User';

const Container = styled.div.withConfig({
  displayName: 'Container',
})`
  align-items: center;
  background-color: #555555;
  color: #d3d3d3;
  display: flex;
  height: 52px;
  padding: 0 20px;

  box-shadow: 1px 1px 3px #444444;
`;

const Title = styled.div.withConfig({
  displayName: 'Title',
})`
  flex: 1 0 auto;
  font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif;
`;

const AuthContainer = styled.div.withConfig({
  displayName: 'AuthContainer',
})`
  align-items: center;
  display: flex;
  font-size: 13px;
`;

const LogButton = styled.button.withConfig({
  displayName: 'LogButton',
})`
  border: none;
  background: transparent;
  border-left: 1px ridge #444;
  color: #d3d3d3;
  height: 42px;
  padding-left: 10px;
`;

const LoginWrapper = styled.span.withConfig({
  displayName: 'LoginWrapper',
})`
  font-size: 12px;
`;

const Login = LogButton.extend`
  border: none;
  color: #bbbbbb;
  padding: 0;

  &:hover {
    color: #d3d3d3;
  }
`;

const Nav = ({ loggedIn }) => (
  <Container>
    <Title>Choose Your Own Adventure Bot</Title>
    {loggedIn ? (
      <AuthContainer>
        <User />
        <LogButton onClick={logout}>Logout</LogButton>
      </AuthContainer>
    ) : (
      <LoginWrapper>
        Please <Login onClick={login}>login</Login> with your discord to use this app
      </LoginWrapper>
    )}
  </Container>
);

Nav.propTypes = {
  loggedIn: PropTypes.bool.isRequired,
};

const mapStateToProps = state => ({
  loggedIn: getLoggedIn(state),
});

export default connect(mapStateToProps)(Nav);
