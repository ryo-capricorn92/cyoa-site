export default {
  'chrome.body': `
    background-color: #666666;
    color: #d3d3d3;
  `,
  'chrome.content': `
    background-color: #666666;
    color: #d3d3d3;
    font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif;
    letter-spacing: 1px;
    display: flex;
    justify-content: center;
    align-items: center;
  `,
  'chrome.nav': `
    background-color: #555555;
  `,
  'chrome.nav_item': props => (props.current ? 'background-color: #444444 !important' : ''),
};
