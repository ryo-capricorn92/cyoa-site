import { applyMiddleware, createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import { createLogger } from 'redux-logger';

import reducer from './reducers';

const configureStore = () => {
  const middleware = [];

  if (process.env.NODE_ENV !== 'production') {
    middleware.push(createLogger({
      collapsed: true,
    }));
  }

  const store = createStore(reducer, composeWithDevTools(applyMiddleware(...middleware)));

  if (module.hot) {
    module.hot.accept('./reducers', () => store.replaceReducer(reducer));
  }

  return store;
};

export default configureStore;
