import { Body, Chrome, Content } from '@zendeskgarden/react-chrome';
import { ThemeProvider } from '@zendeskgarden/react-theming';
import React from 'react';

import theme from '../../styles/theme';

import Header from '../Header/Header';
import Navigation from '../Navigation/Navigation';

const Publish = () => (
  <div>
    <Header />
    <ThemeProvider {...{ theme }} >
      <Chrome>
        <Navigation step="Publish" />
        <Body>
          <Content>
            Coming Soon
          </Content>
        </Body>
      </Chrome>
    </ThemeProvider>
  </div>
);

export default Publish;
