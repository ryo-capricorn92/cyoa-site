export const STEPS = {
  HOME: 'Home',
  PUBLISH: 'Publish',
  SETTINGS: 'Settings',
  STATS: 'Stats',
  STORY: 'Story',
};

export const STEP_ICONS = {
  [STEPS.HOME]: 'fa-home',
  [STEPS.PUBLISH]: 'fa-bolt',
  [STEPS.SETTINGS]: 'fa-cog',
  [STEPS.STATS]: 'fa-bar-chart',
  [STEPS.STORY]: 'fa-book',
};

export default {};
