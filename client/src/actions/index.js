export const setLogin = () => ({
  type: 'LOGIN',
});

export const setLogout = () => ({
  type: 'LOGOUT',
});

export const setStep = step => ({
  type: 'SET_STEP',
  step,
});

export const setUser = user => ({
  type: 'SET_USER',
  user,
});
