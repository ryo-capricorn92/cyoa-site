/* eslint-disable react/jsx-filename-extension */
import PropTypes from 'prop-types';
import React from 'react';
import { connect, Provider } from 'react-redux';
import { BrowserRouter, Redirect, Route } from 'react-router-dom';

import '@zendeskgarden/react-chrome/dist/styles.css';

import { setLogin, setLogout, setUser } from './actions';
import discord from './login';
import { getLoggedIn } from './reducers';

import Home from './components/Home/HomePage';
import Publish from './components/Publish/PublishPage';
import Settings from './components/Settings/SettingsPage';
import Stats from './components/Stats/StatsPage';
import Story from './components/Story/StoryPage';

import './styles/main.css';

class App extends React.Component {
  constructor(props) {
    super(props);
    discord.init(props);

    this.restrictRoute = this.restrictRoute.bind(this);
  }

  restrictRoute(Component) {
    return (...props) => {
      if (!this.props.loggedIn) {
        return <Redirect to="/" />;
      }
      return <Component {...{ props }} />;
    };
  }

  render() {
    return (
      <Provider store={this.props.store}>
        <BrowserRouter>
          <div>
            <Route exact path="/" component={Home} />
            <Route path="/home" render={() => <Redirect to="/" />} />
            <Route path="/stats" render={this.restrictRoute(Stats)} />
            <Route path="/story" render={this.restrictRoute(Story)} />
            <Route path="/publish" render={this.restrictRoute(Publish)} />
            <Route path="/settings" render={this.restrictRoute(Settings)} />
          </div>
        </BrowserRouter>
      </Provider>
    );
  }
}

App.propTypes = {
  loggedIn: PropTypes.bool.isRequired,
  store: PropTypes.shape({}).isRequired,
};

const mapStateToProps = state => ({
  loggedIn: getLoggedIn(state),
});

const mapDispatchToProps = dispatch => ({
  setLogin: () => { dispatch(setLogin()); },
  setLogout: () => { dispatch(setLogout()); },
  setUser: (user) => { dispatch(setUser(user)); },
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
/* eslint-enable react/jsx-filename-extension */
